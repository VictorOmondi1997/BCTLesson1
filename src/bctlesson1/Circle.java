/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bctlesson1;

/**
 *
 * @author Victor Omondi
 * @since 2019
 */
public class Circle extends Shapess{
    //PRIVATE DATA
    double radius;
    //PARAMETERISED CONSTRUCTOR
    public Circle(double radius){
        this.radius = radius;
        calcArea();calcPerimeter();
    }
    @Override
    public void calcArea(){
        double area = radius*radius*22/7;
        System.out.println("Area: "+area);
    }
    @Override
    public void calcPerimeter(){
        double perimeter = radius*2*22/7;
        System.out.println("Perimeter: "+perimeter);
    }
    public static void main(String[] vick){
        Circle circle = new Circle(14.00);
    }
}
