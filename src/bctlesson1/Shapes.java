/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bctlesson1;
import java.util.Scanner;
/**
 *
 * @author Victor Omondi
 */
public class Shapes {
   private static String characters;
   private static byte size;
   private static Scanner cam = new Scanner(System.in);
   public static void getVariables(){
       System.out.println("Enter A Character: (eg: *#^)");
       characters = cam.next();
       System.out.println("Enter Size of Pattern to be Drawn");
       size = cam.nextByte();
       Menu();
   }
   public static void Menu(){
       byte options;
       System.out.println("List of Possible choices");
       System.out.println("1. Square \n2. Triangle \n3. Inverted Triangle \n4. Exit");
       System.out.println("Enter A Number Representing Your Choice");
       options =cam.nextByte();
       switch(options){
           case 1: 
               square(); 
               break;
           case 2: 
               triangle();
               break;
           case 3:
               invertedTriangle();
               break;
           case 4:
               System.exit(0);
               break;
           default:
               System.out.println("!!Invalid Output!!");
           
       }
   }
   public static void square(){
       int i,
           j;
       for(i=1; i<=size; i++){
           for(j=1; j<=size; j++){
               System.out.print(characters);
           }
           System.out.println();
       }
    Menu();
   }
   public static void triangle(){
       int i,
           j;
       for(i=0; i<=size; i++){
           for(j=0; j<=i; j++){
               System.out.print(characters);
           }
        System.out.println();
       }
   Menu();
   }
   public static void invertedTriangle(){
       int i,
           j;
       for(i=size; i>=0; i--){
           for(j=0; j<=i; j++){
               System.out.print(characters);
           }
        System.out.print("\n");
       }  
   Menu();
   }
   public static void main(String[] args){
       getVariables();
   }
}
