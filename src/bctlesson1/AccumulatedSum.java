/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bctlesson1;

/**
 *
 * @author Victor Omondi
 * @see 2019
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class AccumulatedSum {
    JFrame frame = new JFrame(" ");
    JTextField txtEnterInteger = new JTextField(15);
    JLabel lblEnterInteger,lblAccumulatedSum;
    
    int num,sum;
    
    public AccumulatedSum(){
        frame.setSize(300,200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout());
        
        lblEnterInteger = new JLabel("Enter an Integer");
        lblAccumulatedSum = new JLabel();
        
        frame.add(lblEnterInteger);
        frame.add(txtEnterInteger);
        
        txtEnterInteger.setText("5");
        num = Integer.parseInt(txtEnterInteger.getText());
        sum = num+5;
        
        lblAccumulatedSum.setText("Acummulated Sum is "+sum);
        frame.add(lblAccumulatedSum);
        
        frame.setVisible(true);
    }
    public static void main(String[] vick){
        new AccumulatedSum();
    }
}
